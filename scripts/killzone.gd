extends Area2D

@onready var timer = $Timer

func _on_body_entered(body):
	print("You are dead! (X-X)")
	body.get_node("CollisionShape2D").queue_free() # Make the player fall through the map.
	Engine.time_scale = 0.5
	timer.start()

func _on_timer_timeout():
	Engine.time_scale = 1
	print("Reloading...")
	get_tree().reload_current_scene()
