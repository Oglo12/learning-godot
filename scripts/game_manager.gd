extends Node

@onready var score_label = $ScoreLabel

var score = 0

func refresh_score_label():
	score_label.text = "You collected " + str(score) + " coins."
	print("Score: " + str(score))

func add_point():
	score += 1
	refresh_score_label()
